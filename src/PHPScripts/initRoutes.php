<?php

//namespace GMHanciu\ReportsPlatformPHP\PHPScripts;

use GMHanciu\ReportsPlatformPHP\Requirements\PHPRouter\Router;

$router = new Router();
$router->routesPath = dirname(dirname(__FILE__)) . '/Routes/routes.php';
$router->dispatch();
