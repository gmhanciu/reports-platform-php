<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 03-Feb-19
 * Time: 18:31
 */

namespace GMHanciu\ReportsPlatformPHP\Controllers\Test;

use GMHanciu\ReportsPlatformPHP\Controllers\Views\View;

class TestController
{
    public function __construct()
    {
    }

    public function index()
    {
//        $view = new View('testView', __DIR__);
//        $view->assign('message', 'loaded successfully');
//        require __DIR__ . '/testView.php';
        require __DIR__ . '/../../public/reportsPlatformIndex.html';
    }
}