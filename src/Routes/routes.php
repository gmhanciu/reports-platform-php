<?php

//namespace GMHanciu\ReportsPlatformPHP\Routes;

//Route::get('/reports/index', GMHanciu\ReportsPlatformPHP\Controllers\Test\TestController::class . '@index');

if (isset($_SERVER['REQUEST_URI']))
{
    $requestURI = explode('?', $_SERVER['REQUEST_URI'], 2);
}
else
{
    $requestURI[0] = '';
}

switch ($requestURI[0])
{
    case '/reports-platform/index':
        require __DIR__ . '/../dist/index.html';
        //exit required to break the project's 404 rendering page as the route
        //won't be found in the routes stack of the main project
        exit;
        break;
    case '/reports-platform/init-config':
        require PROJECT_ROOT_FOLDER . '/ReportsPlatform/config/initialize.php';
        exit;
        break;
}