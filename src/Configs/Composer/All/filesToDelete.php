<?php

/*
 * CSS
 */
$cssFilesToDelete = [
    "app.*.css",
    "chunk-vendors.*.css",
];
$packageCssFiles = $cssFilesToDelete;

array_walk($cssFilesToDelete, function (&$value, $key) {
    $value = glob(PROJECT_PUBLIC_CSS_FOLDER . "/" . $value);
    //    $value = array_shift($value);
});
$cssFilesToDelete = array_merge(...$cssFilesToDelete);

array_walk($packageCssFiles, function (&$value, $key) {
    $value = glob(PACKAGE_PUBLIC_CSS_FOLDER . "/" . $value);
    //    $value = array_shift($value);
});
$packageCssFiles = array_merge(...$packageCssFiles);

/*
 * JS
 */
$jsFilesToDelete = [
    "app.*.js",
    "app.*.js.map",
    "chunk-vendors.*.js",
    "chunk-vendors.*.js.map",
];
$packageJsFiles = $jsFilesToDelete;

array_walk($jsFilesToDelete, function (&$value, $key) {
    $value = glob(PROJECT_PUBLIC_JS_FOLDER . "/" . $value);
    //    $value = array_shift($value);
});
$jsFilesToDelete = array_merge(...$jsFilesToDelete);

array_walk($packageJsFiles, function (&$value, $key) {
    $value = glob(PACKAGE_PUBLIC_JS_FOLDER . "/" . $value);
    //    $value = array_shift($value);
});
$packageJsFiles = array_merge(...$packageJsFiles);

/*
 * IMG
 */
$imgFilesToDelete = [
    "marc.*.jpg",
    "sidebar-3.*.jpg",
    "rp-logo.*.png",
];
$packageImgFiles = $imgFilesToDelete;

array_walk($imgFilesToDelete, function (&$value, $key) {
    $value = glob(PROJECT_PUBLIC_IMG_FOLDER . "/" . $value);
    //    $value = array_shift($value);
});
$imgFilesToDelete = array_merge(...$imgFilesToDelete);

array_walk($packageImgFiles, function (&$value, $key) {
    $value = glob(PACKAGE_PUBLIC_IMG_FOLDER . "/" . $value);
    //    $value = array_shift($value);
});
$packageImgFiles = array_merge(...$packageImgFiles);

/*
 * All project filenames merged in single array
 */
$projectFiles = array_merge($cssFilesToDelete, $jsFilesToDelete, $imgFilesToDelete);
array_walk($projectFiles, function (&$value, $key) {
    $value = basename($value);
});

/*
 * All package filenames merged in single array
 */
$packageFiles = array_merge($packageCssFiles, $packageJsFiles, $packageImgFiles);
array_walk($packageFiles, function (&$value, $key) {
    $value = basename($value);
});

/*
 * Filenames that are in the main project but not in the package
 */
$filenamesToDelete = array_diff($projectFiles, $packageFiles);

/*
 * All files that need to be deleted
 */
$filesToDelete = array_merge($cssFilesToDelete, $jsFilesToDelete, $imgFilesToDelete);
$filesToDelete = array_filter($filesToDelete, function ($value, $key) use ($filenamesToDelete) {
    return in_array(basename($value), $filenamesToDelete);
}, ARRAY_FILTER_USE_BOTH);

foreach ($filesToDelete as $file)
{
    if ($file)
    {
        unlink($file);
    }
}