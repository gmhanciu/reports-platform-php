<?php

//namespace GMHanciu\ReportsPlatformPHP\Configs\Composer\All;

/*
 * Get public or public_html folder from main project
 */
$packagePublicPath = PACKAGE_PUBLIC_FOLDER;

$packageCSSPath = $packagePublicPath . "/css";
$packageJSPath = $packagePublicPath . "/js";
$packageIMGPath = $packagePublicPath . "/img";

$packageLogoPath = $packagePublicPath . "/rp-logo.png";

$projectPublicFolder = str_replace(PROJECT_ROOT_FOLDER . "/", '', PROJECT_PUBLIC_FOLDER) . "/";

$projectCSSFolder = $projectPublicFolder . "css/";
$projectJSFolder = $projectPublicFolder . "js/";
$projectIMGFolder = $projectPublicFolder . "img/";


//$packageConfigFolderStructure = PACKAGE_ROOT_FOLDER . "/config";
//$projectConfigFolderStructure = "config/";
$packageConfigFolderStructure = PACKAGE_ROOT_FOLDER . "/src/ReportsPlatform";
$projectConfigFolderStructure = "ReportsPlatform";


//$packageConfigPath = PACKAGE_ROOT_FOLDER . "/Configs/Initialize/initialize.php";
//$projectConfigFolder = "config/ReportsPlatform/";

return [
    // Copy public folder to public or public_html of project root
    //    $packagePublicPath => $projectPublicFolder,
    //Copy js/css/img pre-compiled files into public folder of project root
    $packageCSSPath => $projectCSSFolder,
    $packageJSPath => $projectJSFolder,
    $packageIMGPath => $projectIMGFolder,
    //Copy logo to public folder
    $packageLogoPath => $projectPublicFolder,
    // Copy config folders structure
    $packageConfigFolderStructure => $projectConfigFolderStructure,
    // Copy config files in the project config folder
    //    $packageConfigPath => $projectConfigFolder,
];