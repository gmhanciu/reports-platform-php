<?php

$root = null;

// Set the current directory.
// Make sure you set this up so
// that you get out of your own root.
// Assuming this php file is at the root
// of this composer package, this should suffice.
$directory = dirname(dirname(dirname(__FILE__)));

// Go up until you find a composer.json file
// which should exist in the ancestors
// because its a composer package.
do
{
    $directory = dirname($directory);
    $composer = $directory . '/composer.json';
    if(file_exists($composer))
    {
        $root = $directory;

    }
}
while(is_null($root) && $directory != '/');

//// We either are at the root or we got lost.
//// i.e. a composer.json was nowhere to be found.
//if(!is_null($root))
//{
//    // Yay! we are at the root.
//    // and $root contains the path.
//    // Do whatever you seem fit!
//    bootstrapOrSomething();
//}
//else
//{
//    // Oh no! Can we default to something?
//    // Or just bail out?
//    throw new Exception('Oops, did you require this package via composer?');
//}

define('PACKAGE_ROOT_FOLDER', dirname(dirname(dirname(__FILE__))));
define('PROJECT_ROOT_FOLDER', $root);

define('PROJECT_CONFIG_FOLDER', PROJECT_ROOT_FOLDER . "/config/ReportsPlatform");

if (is_dir(PROJECT_ROOT_FOLDER . "/public"))
{
    $projectPublicFolder = "/public";
}
elseif (is_dir(PROJECT_ROOT_FOLDER . "/public_html"))
{
    $projectPublicFolder = "/public_html";
}
else
{
    $errorMessage = "Folder <comment>'public'</comment> or <comment>'public_html'</comment> could not be found in <comment>" . PROJECT_ROOT_FOLDER . "</comment>";
    trigger_error($errorMessage, E_USER_ERROR);
}

define("PROJECT_PUBLIC_FOLDER", PROJECT_ROOT_FOLDER . $projectPublicFolder);

define("PROJECT_PUBLIC_CSS_FOLDER", PROJECT_PUBLIC_FOLDER . "/css");
define("PROJECT_PUBLIC_IMG_FOLDER", PROJECT_PUBLIC_FOLDER . "/img");
define("PROJECT_PUBLIC_JS_FOLDER", PROJECT_PUBLIC_FOLDER . "/js");

define("PACKAGE_PUBLIC_FOLDER", PACKAGE_ROOT_FOLDER . "/src/dist");

define("PACKAGE_PUBLIC_CSS_FOLDER", PACKAGE_PUBLIC_FOLDER . "/css");
define("PACKAGE_PUBLIC_IMG_FOLDER", PACKAGE_PUBLIC_FOLDER . "/img");
define("PACKAGE_PUBLIC_JS_FOLDER", PACKAGE_PUBLIC_FOLDER . "/js");