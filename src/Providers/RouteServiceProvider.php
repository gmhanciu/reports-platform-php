<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 03-Feb-19
 * Time: 20:29
 */

namespace GMHanciu\ReportsPlatformPHP\Providers;

use GMHanciu\ReportsPlatformPHP\Requirements\PHPRouter\Router;
use Composer\Autoload\ClassLoader;

class RouteServiceProvider
{

    private static $routesPath;

    private static $packagePath = '\gmhanciu\reports-platform-php\src\\';

    public function __construct()
    {
    }

    private static function getVendorPath()
    {
        $reflector = new \ReflectionClass(ClassLoader::class);
        $vendorPath = preg_replace('/^(.*)\/composer\/ClassLoader\.php$/', '$1', $reflector->getFileName() );
        if($vendorPath && is_dir($vendorPath)) {
            return $vendorPath . '/';
        }
        throw new \RuntimeException('Unable to detect vendor path.');
    }

    private static function setRoutesPath($vendorDir)
    {
        self::$routesPath = $vendorDir . self::$packagePath;
        return self::$routesPath;
    }

    public static function endsWith($string, $endString)
    {
        $len = strlen($endString);
        if ($len == 0) {
            return true;
        }
        return (substr($string, -$len) === $endString);
    }

    public static function getVendorDir($path)
    {
        while (!self::endsWith($path, '\vendor'))
        {
            $path = dirname($path);
        }
//        return dirname($path);
        return $path;
    }


    public static function createAndDispatch()
    {
//        $vendorDir = self::getVendorDir(dirname(__FILE__));
////        require $vendorDir . '\autoload.php';
//
//        $finalRoutePath = self::setRoutesPath($vendorDir);
//
//        $router = new Router();
//        $router->routesPath = $finalRoutePath . '\routes.php';
//        $router->dispatch();
//
////        return $router;
    }
}