<?php

$config = [
    'DBConnectionConfig' => [
        'DB_CONNECTION' => '',
        'DB_HOST' => '',
        'DB_PORT' => '',
        'DB_DATABASE' => '',
        'DB_USERNAME' => '',
        'DB_PASSWORD' => ''
    ],
    'Homepage' => 'http://gmhrp.develop.eiddew.com/dashboard',
];

if ($_SERVER["REQUEST_URI"] === "/reports-platform/init-config")
{
    echo json_encode($config);
    exit;
}
//if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'))

return $config;
